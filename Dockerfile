FROM php:7.2-fpm

# Copy composer.lock and composer.json
COPY composer.json /var/www/

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get -y install curl
RUN apt-get install -y build-essential && apt-get install -y default-mysql-client && apt-get install -y libpng-dev && apt-get install -y libjpeg62-turbo-dev && apt-get install -y libfreetype6-dev && apt-get install -y locales
RUN apt-get install -y zip &&  apt-get install -y jpegoptim optipng pngquant gifsicle  && apt-get install -y vim && apt-get install -y unzip && apt-get install -y git

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl
RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN docker-php-ext-install gd

#install Imagemagick & PHP Imagick ext and ghostscript
#RUN apt-get update && apt-get install -y \
#        libmagickwand-dev --no-install-recommends \
#        libgs-dev \
#        ghostscript \

#RUN pecl install imagick && docker-php-ext-enable imagick

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Copy config php-fpm
COPY ./php/php-fpm.d/www-app.conf /usr/local/etc/php-fpm.d/

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]

