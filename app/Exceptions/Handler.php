<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Library\Facades\API;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @param Exception $exception
     * @return mixed|void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        try {
            if($exception instanceof NotFoundHttpException && $request->wantsJson()){
                return API::returnJSON($request, (object)[], 400, false, __('API endpoint not found.'));
            }

            if($exception instanceof MethodNotAllowedHttpException){
                return API::returnJSON($request, (object)[], 400, false, __('Http Method not allowed.'));
            }

            if($exception instanceof TokenMismatchException){
                return API::returnJSON($request, (object)[], 400, false, __('Verify csrf token failed.'));
            }

            return parent::render($request, $exception);
        } catch (Exception $exception) {
            return API::catchAPIException($request, $exception);
        }
    }

}
