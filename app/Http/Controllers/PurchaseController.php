<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wager;
use App\Purchase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Rules\checkBuyingPrice;
use Carbon\Carbon;
use App\Library\Facades\API;

/**
 * Class WagerController.
 * @desc Wager Api end point CRUD operation
 */
class PurchaseController extends Controller
{
    /**
     * Store data for a particular resource of Wager
     * @param Request $request
     * @param $wager_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function buy(Request $request, $wager_id)
    {
        try {
            $wager = Wager::find($wager_id);
            if (!$wager) {
                return API::returnJSON($request, (object)[], 404, false, "Can not found wager $wager_id." , [], []);
            }

            $validator = Validator::make($request->all(), [
                'buying_price' => ['required',
                    new checkBuyingPrice($wager)]
            ]);

            if ($validator->fails()) {
                return API::returnJSON($request, (object)[], 422, false, 'Validation fails', [], $validator->errors());
            }


            $purchase = $request->all();
            $purchase['wager_id'] = $wager_id;
            $purchase['bought_at'] = Carbon::now()->toDateTimeString();

            DB::beginTransaction();

            $result = Purchase::create($purchase);
            if ($result) {
                $bought_wager = Purchase::where('wager_id', $wager_id)->sum('buying_price');
                $wager->current_selling_price = $wager->selling_price - $bought_wager;
                $wager->percentage_sold = $bought_wager / (double)$wager->selling_price * 100;
                $wager->amount_sold = (double)$bought_wager;
                $wager->save();
            }

            DB::commit();

            return API::returnJSON($request, $result, 201, false, 'Buy successfully', [], [], []);
        } catch (\Exception $exception) {
            DB::rollBack();
            return API::catchAPIException($request, $exception);
        }
    }
}
