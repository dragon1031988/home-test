<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wager;
use Illuminate\Support\Facades\Validator;
use App\Rules\checkSellingPrice;
use Carbon\Carbon;
use App\Library\Facades\API;

/**
 * Class WagerController.
 * @desc Wager Api end point CRUD operation
 */
class WagerController extends Controller
{
    /**
     * List all the resources for Wager
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $attributes = $request->all();
            $limit = $attributes['limit'] ?: 10;
            $result = Wager::paginate($limit);

            return API::returnJSON($request, $result, 200, false, 'Create new wager successfully', [], [], []);
        } catch (\Exception $exception) {
            return API::catchAPIException($request, $exception);
        }
    }

    /**
     * Store data for a particular resource of Wager
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'total_wager_value' => 'required|integer|min:1',
                'odds' => 'required|integer|min:1',
                'selling_percentage' => 'required|integer|digits_between: 1,100',
                'selling_price' => ['required',
                    new checkSellingPrice($request->all())]
            ]);

            if ($validator->fails()) {
                return API::returnJSON($request, (object)[], 422, false, 'Validation fails', [], $validator->errors());
            }


            $wager = $request->all();
            $wager['current_selling_price'] = $wager['selling_price'];
            $wager['placed_at'] = Carbon::now()->toDateTimeString();

            $result = Wager::create($wager);

            return API::returnJSON($request, $result, 201, false, 'Create new wager successfully', [], [], []);
        } catch (\Exception $exception) {
            return API::catchAPIException($request, $exception);
        }
    }
}
