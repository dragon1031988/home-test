<?php

namespace App\Library;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class API
 * @package App\Library
 */
class API
{
    /**
     * APIFunction constructor.
     */
    public function __construct()
    {
        date_default_timezone_set(config('setting.time_zone', 'UTC'));
    }

    /**
     * Description: Get JSON Response
     * Author: Barry Le <dragon1031988@gmail.com>
     * @param $request
     * @param $data
     * @param $code
     * @param bool $status
     * @param $message
     * @param array $errors
     * @param array $validations
     * @param array $warnings
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnJSON($request, $data, $code, bool $status, $message, $errors = [], $validations = [], $warnings = [])
    {
        $result = (object)[
            'data' => $data,
            'response' => (object)[
                'message' => empty($message) ? '' : $message,
                'errors' => (object)[
                    'validations' => (object)(is_string($validations) ? [$validations] : $validations),
                    'others' => is_string($errors) ? [$errors] : $errors
                ],
                'warnings' => $warnings,
                'status' => $status,
                'code' => $code
            ]
        ];

        try {
            $status_code = [100, 101, 200, 201, 202, 203, 204, 205, 206, 300, 301, 302, 303,
                304, 305, 306, 307, 400, 401, 402, 403, 404, 405, 406, 407, 408,
                409, 410, 411, 412, 413, 414, 415, 416, 417, 429, 500, 501, 502, 503, 504, 505];

            if (!empty($code) && is_numeric($code)) {
                if (in_array($code, $status_code)) {
                    $result->response->code = $code;
                    switch ($code) {
                        case 401:
                            $result->response->message = 'Authentication failed.';
                            $result->response->errors->other = ['The user credentials were incorrect.'];
                            break;
                        default:
                            break;
                    }
                    return response()->json($result, $code);
                }
            }
            return response()->json($result, 400);
        } catch (\Exception $exception) {
            return response()->json([
                'data' => is_object($data) ? (object)[] : [],
                'response' => [
                    'message' => __('auth.something.wrong'),
                    'errors' => [
                        'validations' => [],
                        'other' => [$exception->getMessage()]
                    ],
                    'warnings' => [],
                    'status' => false,
                    'code' => 400
                ]
            ], 400);
        }
    }

    /**
     * Description: Logging APIs
     * Author: Barry Le <dragon1031988@gmail.com>
     * @param $request
     * @param $code
     * @param $result
     */
    public function logging($request, $code, $result)
    {
        $method = strtoupper($request->getMethod());
        $uri = $request->getPathInfo();
        $bodyAsJson = json_encode($request->except(config('http-logger.except')));
        $files = array_map(function (UploadedFile $file) {
            return $file->getRealPath();
        }, iterator_to_array($request->files));

        $result_json = json_encode($result);
        $message = "{$method} {$uri} - Status: {$code} - Result: {$result_json} - Body: {$bodyAsJson} - Files: " . implode(', ', $files);

        Log::channel('daily')->info($message);
    }

    /**
     * Description: Catch API Exceptions and Return JSON Response
     * Author: Barry Le <dragon1031988@gmail.com>
     * @param $rq
     * @param $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function catchAPIException($rq, $e)
    {
        if ($e instanceof HttpException) {
            return $this->returnJSON($rq, (object)[], $e->getStatusCode(), false, __('auth.something.wrong'), $e->getMessage());
        }
        if ($e->getCode()) {
            return $this->returnJSON($rq, (object)[], $e->getCode(), false, __('auth.something.wrong'), $e->getMessage());
        }
        return $this->returnJSON($rq, (object)[], 400, false, __('auth.something.wrong'), $e->getMessage());
    }
}
