<?php
/**
 * Created by PhpStorm.
 * User: Azure Cloud
 * Date: 4/18/2018
 * Time: 2:42 PM
 */

namespace App\Library\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Illuminate\Http\JsonResponse execute($api, $data = [], $uri_params = [])
 * @method static \Illuminate\Http\JsonResponse returnJSON($request, $data, $code, $status, $message, $error = '', $validations = null)
 * @method static void logging($request, $code, $result)
 * @method static \Illuminate\Http\JsonResponse catchAPIException($rq, $e)
 *
 */
class API extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Library\API::class;
    }

}