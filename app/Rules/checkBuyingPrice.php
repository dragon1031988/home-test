<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class checkBuyingPrice implements Rule
{
    private $wager = [];

    /**
     * Create a new rule instance.
     * checkBuyingPrice constructor.
     * @param $wager
     */
    public function __construct($wager)
    {
        $this->wager = $wager;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value <= $this->wager->current_selling_price;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Buying price must be lesser or equal to current selling price';
    }
}
