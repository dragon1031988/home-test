<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class checkSellingPrice implements Rule
{
    private  $param = [];

    /**
     * checkSellingPrice constructor.
     * @param $param
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $total_wager_value = (int)$this->param['total_wager_value'];
        $selling_percentage = (int)$this->param['selling_percentage'];
        $selling_price = (int)$value;
        return $selling_price < ($total_wager_value * ($selling_percentage / 100));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Selling Price must be greater than total_wager_value * (selling_percentage / 100)';
    }
}
