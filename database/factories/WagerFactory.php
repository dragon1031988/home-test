<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//User table data seeder
$factory->define(App\Wager::class, function (Faker $faker) {

    return [
        'total_wager_value' => 1000,
        'odds' => 30,
        'selling_percentage' => 80,
        'selling_price' => 100,
        'current_selling_price' => 100,
        'placed_at' => now(),
    ];
});
