<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');


Route::group(['middleware' => 'auth:api'], function() {

	Route::get('/user', function (Request $request) {
    	return $request->user();
    });

    Route::get('wagers', 'WagerController@index');
    Route::post('wagers', 'WagerController@store');

    Route::post('purchase/buy/{wager_id}', 'PurchaseController@buy');
});
