<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Wager;
use App\User;

class PurchaseTest extends TestCase
{

	/**
     * Test Case for create new Purchase Api.
     *
     * @return void
     */
    public function testsPurchasesAreCreatedCorrectly()
    {
    	//create a test user
        $user = factory(User::class)->create();
        //get the access token
        $token = $user->generateToken();
        //Bearer access token header
        $headers = ['Authorization' => "Bearer $token"];

        // create wager
        $wager = factory(Wager::class)->create();
        $tData = ["buying_price" => 60];

        //test Purchases api
        $this->json('POST', "/api/purchase/buy/$wager->id", $tData, $headers)
            ->assertStatus(201)
            ->assertJsonStructure(['data' => [ 'id', 'buying_price', 'wager_id', 'bought_at', 'updated_at', 'created_at']]);
    }
}
