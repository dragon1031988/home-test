<?php

namespace Tests\Feature;

use App\Wager;
use Tests\TestCase;
use App\User;

class WagerTest extends TestCase
{

	/**
     * Test Case for create new Purchase Api.
     *
     * @return void
     */
    public function testsPurchasesAreCreatedCorrectly()
    {
    	//create a test user
        $user = factory(User::class)->create();
        //get the access token
        $token = $user->generateToken();
        //Bearer access token header
        $headers = ['Authorization' => "Bearer $token"];
        //test data
        $tData = [
            "total_wager_value" => 1000,
            "odds" => 10,
            "selling_percentage" => 10,
            "selling_price"=> 80
        ];


        //test wagers create new api
        $this->json('POST', '/api/wagers', $tData, $headers)
            ->assertStatus(201)
            ->assertJson(['data' =>[
                    'id' => 1,
                    'total_wager_value' => 1000,
                    'odds' => 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> 80,
                    'current_selling_price'=> 80,
            ]]);
    }

    /**
     * Test Case for listing Subscriber Api.
     *
     * @return void
     */
    public function testWagersAreListedCorrectly()
    {
        factory(Wager::class)->create([
            'total_wager_value' => 1000,
            'odds' => 30,
            'selling_percentage' => 80,
            'selling_price' => 100,
            'current_selling_price' => 100,
            'placed_at' => now(),
        ]);

        factory(Wager::class)->create([
            'total_wager_value' => 2000,
            'odds' => 60,
            'selling_percentage' => 160,
            'selling_price' => 200,
            'current_selling_price' => 200,
            'placed_at' => now(),
        ]);

        //create test User by user factory call
        $user = factory(User::class)->create();
        //get user token
        $token = $user->generateToken();
        //composer the bearer access token
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('GET', '/api/wagers?page=1&limit=10', [], $headers)
            ->assertStatus(200)
            ->assertJson(['data' =>
                [ 'data' => [
                    [
                        'total_wager_value' => 1000,
                        'odds' => 30,
                        'selling_percentage' => 80,
                        'selling_price' => 100,
                        'current_selling_price' => 100,
                    ],
                    [
                        'total_wager_value' => 2000,
                        'odds' => 60,
                        'selling_percentage' => 160,
                        'selling_price' => 200,
                        'current_selling_price' => 200,
                    ]
                ]
            ]]);
    }
}
